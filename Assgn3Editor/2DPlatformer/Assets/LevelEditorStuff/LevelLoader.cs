﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorToPrefab{
	public Color32 color;
	public GameObject prefab;
}


public class LevelLoader : MonoBehaviour {

	public Texture2D levelMap;
	string levelName;
	public List<string> levelNames = new List<string>();
    //Alvin, in this class, change the add buttons function 
    //empty the list
    //call your function that gives you the array of level names
    //set levelNames to this array
    // 
    //That should change the buttons to whatever was in the text file. These names have to correspond with the texture in resource folder
    //
    //You can then change the LevelEditor code to send strings (the elements in the list) instead of ints
    //Also the button text can be set to the elements in the string

	public ColorToPrefab[] colorToPrefab;

	// Use this for initialization
	void Start () {
		levelName = "Example1";

		levelNames.Add ("Example1");
		levelNames.Add ("Example2");

		levelMap = (Texture2D)Resources.Load(levelNames[1]);
		LoadMap ();
	}
	
	void EmptyMap() //Find all created elements, delete them.
	{
		while (transform.childCount > 0) {
			Transform c = transform.GetChild (0);
			c.SetParent (null); //Take the parent away to stop inifinite loop
			Destroy (c.gameObject); //Delete object
		}
	}

	void LoadMap()
	{
		EmptyMap ();

		//Get pixels from image
		Color32[] allPixels = levelMap.GetPixels32();
		int width = levelMap.width;
		int height = levelMap.height;

		for (int x = 0; x <width; x++){
			for (int y = 0; y < height; y++) {
				SpawnTileAt(allPixels[(y * width) + x], x, y); //array is one dimensional. This 2d's it
			}
		}
	}

	void SpawnTileAt(Color32 c, int x, int y)
	{
		//If this is transparent, skip all this
		if (c.a <= 0) {  return;	}
		// Find the right colour in our map
		foreach (ColorToPrefab ctp in colorToPrefab)
		{
			//if(ctp.color.r == c.r && ctp.color.b == c.b && ctp.color.g == c.g && ctp.color.a == c.a)
			if(c.Equals(ctp.color)){
				//Spawn prefab
				GameObject go = (GameObject)Instantiate(ctp.prefab, new Vector3(x,y,0), Quaternion.identity);
				go.transform.parent = transform;
				//maybe do more
				return;
			}
		}
		//If we got to this, a colour is wrong
		Debug.Log ("Colour is not found for: " + c.ToString());
	}

	public void SetLevel(int _levSel)
	{
		levelName = "Example" + _levSel;

		resetMap ();
	}

	public void SetLevel(string _levSel)
	{
		levelName = _levSel;

		resetMap ();
	}

	public void AddLevel()
	{
		int i = levelNames.Count;
		i++;
		levelNames.Add ("Example" + i);
	}

	void resetMap()
	{
		levelMap = (Texture2D)Resources.Load(levelName);
		LoadMap ();	
	}
}