﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnitySampleAssets._2D
{
	public class LevelEditor : EditorWindow 
	{
		public int levSel = 0;
		GameObject level;

		[MenuItem("Window/LevelEditor")]

		public static void OurCustomWindow()
		{
			EditorWindow.GetWindow(typeof(LevelEditor));
		}

		void OnGUI()
		{
			level = GameObject.Find("Level");

			if (!level)
				Debug.Log ("why u no work");

			GUILayout.Label ("Level Selection", EditorStyles.boldLabel);


			//Too add more levels, first add the png to the resource folder, and modify it to the same texture stats as the examples. 
			//FILE MUST BE NAMED "Example" + i IN CHRONOLOGICAL ORDER
			for (int i = 0; i < level.GetComponent<LevelLoader> ().levelNames.Count; i++) {
				if (GUILayout.Button ("Change to level " + (i+1))) {
					levSel = i + 1;
					level.GetComponent<LevelLoader> ().SetLevel (levSel);
				}
			}

			if (GUILayout.Button ("Add more level options! (Only press if \"Example\" ^+1 is in your 'Resources' Folder ")) {
				level.GetComponent<LevelLoader> ().AddLevel ();
			}
				
		}
			


	}
}
