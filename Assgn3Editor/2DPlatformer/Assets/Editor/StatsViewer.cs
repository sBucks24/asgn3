﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnitySampleAssets._2D
{
    public class StatsViewer : EditorWindow {

        GameObject player;

    [MenuItem("Window/StatsViewer")]

        public static void OurCustomWindow()
        {
            EditorWindow.GetWindow(typeof(StatsViewer));
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        void OnGUI()
        {
            player = GameObject.FindGameObjectWithTag("Player");

            if (player)
            {
                GUILayout.Label("Stats:", EditorStyles.boldLabel);
                player.GetComponent<Stats>().score = EditorGUILayout.IntField("Score: ", player.GetComponent<Stats>().score);
                player.GetComponent<Stats>().timesDead = EditorGUILayout.IntField("Deaths: ", player.GetComponent<Stats>().timesDead);
            }
        }

    }
}