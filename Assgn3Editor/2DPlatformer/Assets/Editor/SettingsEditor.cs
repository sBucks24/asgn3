﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnitySampleAssets._2D
{
    public class SettingsEditor : EditorWindow
    {
        GameObject player;

        [MenuItem("Window/SettingsEditor")]

        public static void OurCustomWindow()
        {
            EditorWindow.GetWindow(typeof(SettingsEditor));
        }

        void OnGUI()
        {
            player = GameObject.FindGameObjectWithTag("Player");

            if (player)
            {
                GUILayout.Label("Player Settings:", EditorStyles.boldLabel);
                player.GetComponent<Settings>().p.maxSpeed = EditorGUILayout.FloatField("Max Speed: ", player.GetComponent<Settings>().p.maxSpeed);
                player.GetComponent<Settings>().p.jumpForce = EditorGUILayout.FloatField("Jump Force: ", player.GetComponent<Settings>().p.jumpForce);
                player.GetComponent<Settings>().p.mass = EditorGUILayout.FloatField("Mass: ", player.GetComponent<Settings>().p.mass);
                player.GetComponent<Settings>().p.gravityMultiplier = EditorGUILayout.FloatField("Gravity Multiplier: ", player.GetComponent<Settings>().p.gravityMultiplier);
                GUILayout.Label("Camera Settings:", EditorStyles.boldLabel);
                player.GetComponent<Settings>().p.dampening = EditorGUILayout.FloatField("Camera Dampening: ", player.GetComponent<Settings>().p.dampening);
                player.GetComponent<Settings>().p.lookFactor = EditorGUILayout.FloatField("Camera Look Forward Factor: ", player.GetComponent<Settings>().p.lookFactor);
                player.GetComponent<Settings>().p.lookReturn = EditorGUILayout.FloatField("Camera Look Return Speed: ", player.GetComponent<Settings>().p.lookReturn);
                player.GetComponent<Settings>().p.lookThreshold = EditorGUILayout.FloatField("Camera Look Distance: ", player.GetComponent<Settings>().p.lookThreshold);
                //player.GetComponent<Settings>().
                player.GetComponent<Settings>().setSettings();
            }
        }

    }
}