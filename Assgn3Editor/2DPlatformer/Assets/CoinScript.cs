﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnitySampleAssets._2D
{
public class CoinScript : MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
			if (player == null) {
				player = GameObject.Find ("Player(Clone)");
			}

	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
                player = other.gameObject;
                if (gameObject.activeSelf)
                {
                    player.GetComponent<Stats>().score++;
                    player.GetComponent<Stats>().addScore();
                }

			gameObject.SetActive (false);
		}
	}
}
}