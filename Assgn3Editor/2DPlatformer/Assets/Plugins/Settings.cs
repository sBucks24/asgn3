﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace UnitySampleAssets._2D
{
    public class Settings : MonoBehaviour
    {

        //[StructLayout(LayoutKind.Sequential)]
        public struct Player
        {
            public float maxSpeed, jumpForce, mass, gravityMultiplier, dampening, lookFactor, lookReturn, lookThreshold;
        }


        [DllImport("RigidBodyPlugin")]
        public static extern void initialize();

        [DllImport("RigidBodyPlugin")]
        public static extern void updatePlayer(float _speed, float _force, float _mass, float _grav);

        [DllImport("RigidBodyPlugin")]
        public static extern void updateCameraSettings(float _damp, float _lFactor, float _lReturn, float _lThreshold);

        [DllImport("RigidBodyPlugin")]
        public static extern Player getPlayerSettings();

        [DllImport("RigidBodyPlugin")]
        public static extern void writeToFile();

        public GameObject player;
        public Rigidbody2D pBody;
        public Camera mainCam;
        public Player p;

        // Use this for initialization
        void Start()
        {
            player = this.gameObject;
            pBody = this.gameObject.GetComponent<Rigidbody2D>();
            mainCam = Camera.main;
            initialize();
            p = getPlayerSettings();
            updatePlayerSettings();
            updateCamera();

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                initialize();
                p = getPlayerSettings();
                print(p.maxSpeed);
                updatePlayerSettings();
                updateCamera();
            }

        }

        void updatePlayerSettings()
        {
            pBody.mass = p.mass;
            pBody.gravityScale = p.gravityMultiplier;
            player.GetComponent<PlatformerCharacter2D>().maxSpeed = p.maxSpeed;
            player.GetComponent<PlatformerCharacter2D>().jumpForce = p.jumpForce;

        }

        void updateCamera()
        {
            mainCam.GetComponent<Camera2DFollow>().lookAheadFactor = p.lookFactor;
            mainCam.GetComponent<Camera2DFollow>().damping = p.dampening;
            mainCam.GetComponent<Camera2DFollow>().lookAheadReturnSpeed = p.lookReturn;
            mainCam.GetComponent<Camera2DFollow>().lookAheadMoveThreshold = p.lookThreshold;
        }

        public void setSettings()
        {
           updatePlayer(p.maxSpeed, p.jumpForce, p.mass, p.gravityMultiplier);
           updateCameraSettings(p.dampening, p.lookFactor, p.lookReturn, p.lookThreshold);
           writeToFile();
           p = getPlayerSettings();
           updatePlayerSettings();
           updateCamera();
           print("doing stuff");

        }
    }
}
