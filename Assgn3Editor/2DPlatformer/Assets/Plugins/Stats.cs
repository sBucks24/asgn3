﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


namespace UnitySampleAssets._2D
{
    public class Stats : MonoBehaviour
    {

        [DllImport("DebugPlugin")]
        public static extern void initialize();

        [DllImport("DebugPlugin")]
        public static extern void updateScore(int _score);

        [DllImport("DebugPlugin")]
        public static extern void updateTimesDead();

        [DllImport("DebugPlugin")]
        public static extern int getScore();

        [DllImport("DebugPlugin")]
        public static extern int getDeaths();

        [DllImport("DebugPlugin")]
        public static extern void writeToFile();

        public int score;
        public int timesDead;
        public bool isDead;
        public GameObject killzone;
        public GameObject player;

        // Use this for initialization
        void Start()
        {
            initialize();
            score = getScore();
            timesDead = getDeaths();
            killzone = GameObject.FindGameObjectWithTag("Respawn");
            player = GameObject.FindGameObjectWithTag("Player");
            isDead = false;
        }

        // Update is called once per frame
        void Update()
        {
            

            if (isDead)
            {
				//updateScore(score);
                updateTimesDead();
                writeToFile();
                print("death");
                Application.LoadLevel(Application.loadedLevelName);
            }
        }

        public void addScore()
        {
            updateScore(1);
            writeToFile();

        }
    }
}