﻿using UnityEngine;

namespace UnitySampleAssets._2D
{
    public class Restarter : MonoBehaviour
    {
        public GameObject player;

        void Start()
        {
			//player = GameObject.Find("Player(Clone)");
            //player.GetComponent<Stats>().isDead = false;
        }

		void Update()
		{
			if (player == null) {
				player = GameObject.Find ("Player(Clone)");
                player.GetComponent<Stats>().isDead = false;
            }
		}

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                player.GetComponent<Stats>().isDead = true;           
            }
        }
    }
}