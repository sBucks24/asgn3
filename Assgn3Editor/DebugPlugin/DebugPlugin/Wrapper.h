#pragma once


extern "C"
{
	__declspec(dllexport) void initialize();
	__declspec(dllexport) void updateScore(int _score);
	__declspec(dllexport) void updateTimesDead();
	__declspec(dllexport) int getScore();
	__declspec(dllexport) int getDeaths();
	__declspec(dllexport) void writeToFile();
}
