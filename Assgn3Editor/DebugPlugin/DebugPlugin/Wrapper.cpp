#include "Wrapper.h"
#include<fstream>
#include<iostream>

std::ofstream outFile;
std::ifstream inFile;
int score;
int timesDead;

void initialize()
{
	inFile.open("stats.txt");
	
	inFile >> score >> timesDead;

	inFile.close();
}
void updateScore(int _score)
{
	score += _score;
}

void updateTimesDead()
{
	timesDead++;
}

int getScore()
{
	return score;
}
int getDeaths()
{
	return timesDead;
}

void writeToFile()
{

	outFile.open("stats.txt");
	outFile << score << std::endl;
	outFile << timesDead << std::endl;
	outFile.close();
}