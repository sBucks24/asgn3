#pragma once
#include <vector>

class Player
{
public:
	Player();
	~Player();
	void setPlayerSettings(float _max, float _force, float _mass, float _grav);
	void setCamera(float _damp, float _lFactor, float _lReturn, float _lThreshold);
	std::vector<float> Player::getVectorSettings();
private:
	float maxSpeed;
	float jumpForce;

	float mass;
	float gravityMultiplier;

	float cameraDamp;
	float lookFactor;
	float lookReturn;
	float lookThreshold;
};