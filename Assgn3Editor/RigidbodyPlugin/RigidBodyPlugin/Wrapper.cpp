#include "Wrapper.h"


#include<fstream>
#include<iostream>
#include <string>
#include <vector>

std::ofstream outFile;
std::ifstream inFile;
std::vector<float> data;
std::vector<float> pData;
float number;
Player p;

void initialize()
{
	inFile.open("settings.txt");


	if (data.size() < 8)
	{
		data.clear();
		for (int i = 0; i < 8; i++)
		{
			data.push_back(0.0f);
			inFile >> data[i];
		}
	}
	else
	{
		for (int i = 0; i < 8; i++)
		{
			inFile >> data[i];
		}
	}

	inFile.close();

	if (data.size() == 8)
	{
		p.setPlayerSettings(data[0], data[1], data[2], data[3]);
		p.setCamera(data[4], data[5], data[6], data[7]);
	}

}
	
void updatePlayer(float _speed, float _force, float _mass, float _grav)
{
	p.setPlayerSettings(_speed, _force, _mass, _grav);
}

void updateCameraSettings(float _damp, float _lFactor, float _lReturn, float _lThreshold)
{
	p.setCamera(_damp, _lFactor, _lReturn, _lThreshold);
}

Player getPlayerSettings()
{
	return p;
}

void writeToFile()
{
	pData = p.getVectorSettings();
	outFile.open("settings.txt");
	for (int i = 0; i < pData.size(); i++)
	{
		outFile << pData[i] << std::endl;
		std::cout << pData[i] << std::endl;
	}
	outFile.close();
}