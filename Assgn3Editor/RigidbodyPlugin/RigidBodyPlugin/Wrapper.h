#pragma once
#include "CharacterClass.h"

extern "C"
{
	__declspec(dllexport) void initialize();
	__declspec(dllexport) void updatePlayer(float _speed, float _force, float _mass, float _grav);
	__declspec(dllexport) void updateCameraSettings(float _damp, float _lFactor, float _lReturn, float _lThreshold);
	__declspec(dllexport) Player getPlayerSettings();
	__declspec(dllexport) void writeToFile();
	
}