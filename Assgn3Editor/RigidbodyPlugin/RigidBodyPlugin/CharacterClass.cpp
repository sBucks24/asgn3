#include "CharacterClass.h"

Player::Player()
{
	maxSpeed = 10.0f;
	jumpForce = 800.0f;
	cameraDamp = 0.2f;
	lookFactor = 1.5f;
	lookReturn = 0.75f;
	lookThreshold = 0.1f;
}
Player::~Player()
{

}
void Player::setPlayerSettings(float _max, float _force, float _mass, float _grav)
{
	if(_max > 0)
		maxSpeed = _max;
	if (_force > 0)
		jumpForce = _force;
	if (_mass > 0)
		mass = _mass;
	if (_grav > 0)
		gravityMultiplier = _grav;
}

void Player::setCamera(float _damp, float _lFactor, float _lReturn, float _lThreshold)
{
	if(_damp > 0)
		cameraDamp = _damp;
	if(_lFactor > 0)
		lookFactor = _lFactor;
	if(_lReturn > 0)
		lookReturn = _lReturn;
	if(_lThreshold > 0)
		lookThreshold = _lThreshold;
}

std::vector<float> Player::getVectorSettings()
{
	std::vector<float> player;
	player.push_back(maxSpeed);
	player.push_back(jumpForce);
	player.push_back(mass);
	player.push_back(gravityMultiplier);
	player.push_back(cameraDamp);
	player.push_back(lookFactor);
	player.push_back(lookReturn);
	player.push_back(lookThreshold);

	return player;
}